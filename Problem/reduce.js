const ReduceMethod = (items, callback) => {
  //it is inital value of our accumulator
  let initalvalue = 0;

  //it will loop through dour items array
  for (let index = 0; index < items.length; index++) {
    //here we will store the  accumulated value
    initalvalue = callback(items[index], initalvalue, items);
  }

  //now we will return our result
  return initalvalue;
};

module.exports = ReduceMethod;
