const FlattenMethod = (items) => {
  //it will be our flatten array
  const result = [];

  const Flat = (items) => {
    //here we are looping our array
    for (let index = 0; index < items.length; index++) {
      //here we are checking if element is  array the we are recursive pass this array
      if (Array.isArray(items[index])) {
        Flat(items[index]);
        //  index--;
      } else {
        //otherwise we will push result into our flatten array
        result.push(items[index]);
      }
    }
  };

  Flat(items);

  return result;
};

module.exports = FlattenMethod;
