//for Each will help to  itrate over each element and apply a callback fn to each element and perform some operation

const ForEachMethod = (items, callback) => {
  for (index = 0; index < items.length; index++) {
    //here we are calling a callback fn on each element
    callback(items[index], index, items);
  }
};

module.exports = ForEachMethod;
