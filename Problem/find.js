const FindMethod = (items, callback) => {
  //so this will be our result array
  let result = [];

  //here we are iterating over in items array
  for (let index = 0; index < items.length; index++) {
    //here we are calling our callback function
    if (callback(items[index], index, items)) {
      //here we are checking if its is true then we will push it into our  result array
      result.push(items[index]);
    }
  }

  //here we returning the result array elements hich are find int the element
  return result;
};

module.exports = FindMethod;
