//Map method is a higher-order function which is  used to loop  over each elements of an array and  it bascially apply callback fn to each element and return the result into  new array

//so it is our custom map method it will take two arguments first is array and second is a callback fn
const mapMethod = (items, callback) => {
  //here we have initalize our result array
  let result = [];

  //this is used to iterate over each element
  for (let index = 0; index < items.length; index++) {
    //suppose we want to make a mapping for each element to be double

    // so here we are calling a callback fn for each element so like map we will pass arguments
    result.push(callback(items[index], index, items));
    // like first will be the element of array second index , abd at last we will pass the whole array
  }

  return result;
};

module.exports = mapMethod;
