//Map method is a higher-order function which is  used to loop  over each elements of an array and  it bascially apply callback fn to each element and callbackfn basically do a filtration based on some condition

const FilterMethod = (items, callback) => {
  //here we have initalize the empty result array
  let result = [];

  //used for looping over array
  for (index = 0; index < items.length; index++) {
    //here we are calling a callback fn on each element
    // like first will be the element of array second index , abd at last we will pass the whole array
    if(callback(items[index],index, items))
    {
        result.push(items[index]);

    }

}

    //so we will return the result array
    return result;

};


module.exports=FilterMethod;