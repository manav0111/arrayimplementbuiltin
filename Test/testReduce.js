const ReduceMethod = require("../Problem/reduce");

//so this is our item array
const items = [1, 2, 3, 4, 5, 5];

//let see the example of Sum Method callback function
const Sum = (element, accum) => {
  return element + accum;
};

//it will give the Result Sum:
console.log(ReduceMethod(items, Sum));
