const FindMethod=require('../Problem/find');

//so this is our item array
const items = [1, 2, 3, 4, 5, 5]; 

//callback function 
const OddNumber=(element)=>{

    return element%2!==0;

}

//here we are printing the elements we find
console.log(FindMethod(items,OddNumber));



