const mapMethod=require('../Problem/map');

//so this is our item array
const items = [1, 2, 3, 4, 5, 5]; 

//so this is our callback function
const Double=(element)=>{
    return element*2;
}

//it will give us output like map
console.log(mapMethod(items,Double));