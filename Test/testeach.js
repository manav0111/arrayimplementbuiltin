const ForEachMethod = require("../Problem/each")

//so this is our item array
const items = [1, 2, 3, 4, 5, 5]; 

//so this is our callback function
const Print=(element)=>{
    console.log(element);
}

//it will give us output like forEach
ForEachMethod(items,Print);